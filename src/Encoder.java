import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import java.util.*;


public class Encoder {

	static int count_1;
	static HashMap <String, String> encds2;
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		File file;

		file = new File(args[0]);

		Scanner scan = new Scanner(file);
		int frequencies[]= new int[26];
		int[] charFreqs = new int[256];

		int i =0;
		int TOTAL = 0;
		int letter = 65;
		while (scan.hasNextLine()) {
			int num = scan.nextInt();
			frequencies[i] = num;
			charFreqs[letter] = num;
			TOTAL +=num;
			++i;
			++letter;
		}
		scan.close();
		HashMap<Integer, Double> probabilites = new HashMap<Integer,Double>();
		double h = 0;
		for(int symbol : frequencies){

			double x =  symbol/ (double)TOTAL;
			probabilites.put(symbol, x);
			if(x > 0){
				h +=  (x * Math.log((double) x)/Math.log(2));
			}
		}
		h = -h;

		int k = Integer.parseInt(args[1]);

	/*
	 * One Symbol
	 */
		HashMap <String, Integer> freq1 = new HashMap <String, Integer>();
		HashMap <String, String> encds = new HashMap <String, String>();
		HashMap <String, String> encdsr = new HashMap <String, String>();

		HuffmanTree tree = HuffmanCode.buildTree(charFreqs, freq1, 1);

		// print out results
		System.out.println("SYMBOL\tWEIGHT\tHUFFMAN CODE");
		HuffmanCode.printCodes(tree, new StringBuffer(),encds,encdsr);
		volume(frequencies, TOTAL, k, 1);
		encode(encds, 1);
		decode(encdsr, 1);
		
		//Results on 1-symbols
		double average1 = count_1/(double)k;
		System.out.println("Symbol count: " + count_1 );
		System.out.println("Entropy: " + h);
		System.out.println("Average = " + average1 + "bits/symbol");
		System.out.println("Percentage Difference = %" + (100*(average1 - h)/(h)) + "\n\n");
		count_1 =0;
		
		/*
		 * Symbol 2
		 */
		int []charFreqs2 = new int[26*26];

		HashMap <String, Integer> freq2 = new HashMap <String, Integer>();
		encds2 = new HashMap <String, String>();
		HashMap <String, String> encdsr2 = new HashMap <String, String>();
		//char frequence for two

		int sum2 = 0;
		for(int m = 0 ; m < frequencies.length; m++){
			for(int n = 0; n < frequencies.length; n++){
				int i_r = (m * 26) + n;
				charFreqs2[i_r] = frequencies[n] * frequencies[m];
				sum2 += charFreqs2[i_r];
			}
		}
		HuffmanTree tree2 = HuffmanCode.buildTree(charFreqs2, freq2, 2);
		System.out.println("SYMBOL\tWEIGHT\tHUFFMAN CODE");
		HuffmanCode.printCodes(tree2, new StringBuffer(),encds2,encdsr2);
		volume(charFreqs2, sum2, k, 2);
		encode(encds2, 2);
		decode(encdsr2, 2);

		//Results on 2-symbols
		average1 = count_1/(double)k;
		System.out.println("Symbol count: " + count_1 );
		System.out.println("Entropy: " + h);
		System.out.println("Average = " + average1 + "bits/symbol");
		System.out.println("Percentage Difference = %" + (100*(average1 - h)/(h)) + "\n\n");
	}

	private static void volume2(String[] charFreqs, int k, int js) throws IOException {
		BufferedWriter testText = new BufferedWriter(new FileWriter("testText" + js));
		int i, ran;
		Random rand = new Random();

		for (i = 0; i < k;) {
			ran = rand.nextInt(charFreqs.length);
			if( charFreqs[ran] != null){
				testText.write("" +charFreqs[ran]);
				i++;
			}

		}
		testText.close();

	}
	private static void volume(int[] charFreqs, int total, int k, int js) throws IOException {
		BufferedWriter testText;


		if(js>1){
			volume2(encds2.keySet().toArray(new String[26*26]), k , js);

		}else{
			testText = new BufferedWriter(new FileWriter("testText"));
			Random rand = new Random();

			int count; 
			int r ;
			int m ;	

			for (int i = 0; i < k; i++)
			{
				r = rand.nextInt(total);
				m = 0;	
				count = 0;

				for(; count <= r; m++){
					count += charFreqs[m];
				}	
				if (js == 1){
					char l = (char)(65 + m-1);
					testText.write(l);
				}
				else 									
				{
						js--;			
				}
			}
			testText.close();
		}

	}


	public static void encode (Map <String, String> encodings, int j) throws IOException {
		BufferedWriter enc;
		if(j>1){
			enc = new BufferedWriter(new FileWriter("testText.enc" + j));
		}else{
			enc = new BufferedWriter(new FileWriter("testText.enc1"));
		}
		BufferedReader testText;
		if(j > 1){
			testText = new BufferedReader(new InputStreamReader(new FileInputStream("testText2")));
		}
		else{
			testText = new BufferedReader(new InputStreamReader(new FileInputStream("testText")));

		}
		int read;
		int i = 1;
		String s = "";
		while((read = testText.read()) != -1) 
		{
			char character = (char) read;
			s += character;

			if(i++ == j){
				enc.write(encodings.get(s)+'\n');
				s= "";	
				i=1;
			}
		}
		enc.close();
		testText.close();
	}

	public static void decode(Map <String, String> encodingsr, int num_symbols) throws IOException {
		BufferedWriter dwrite;
		if(num_symbols>1){
			dwrite = new BufferedWriter(new FileWriter("testText.dec"+ num_symbols) );
		}else{
			dwrite = new BufferedWriter(new FileWriter("testText.dec1"));
		}
		count_1 = 0;
		String symbol;
		Scanner scan = new Scanner(new File("testText.enc" + num_symbols));
		BufferedReader d = new BufferedReader(new InputStreamReader(new FileInputStream("testText.enc" + num_symbols)));
		while(d.read() != -1){
			count_1++;
		}
		d.close();
		while (scan.hasNextLine() ) {
			String e = scan.nextLine();
			symbol = encodingsr.get(e);
			dwrite.write(symbol);
		}
		dwrite.close();
		scan.close();
	}

}
