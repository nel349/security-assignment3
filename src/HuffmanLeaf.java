class HuffmanLeaf extends HuffmanTree {
    public final String value; // the character this leaf represents
 
    public HuffmanLeaf(int freq, String val) {
        super(freq);
        value = val;
    }
    
//    public String to_string(){
//    	System.out.println(value);
//    	return Character.toString(value);
//    }
}