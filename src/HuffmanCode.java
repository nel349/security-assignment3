import java.util.Map;
import java.util.PriorityQueue;

public class HuffmanCode {
    // input is an array of frequencies, indexed by character code
	public static HuffmanTree buildTree(int[] charFreqs, Map <String, Integer> stringFreqs, int num_symbols) {
        PriorityQueue<HuffmanTree> trees = new PriorityQueue<HuffmanTree>();
        // initially, we have a forest of leaves
        // one for each non-empty character
        if (num_symbols == 1)
        {
            for (int i = 65; i < charFreqs.length; i++)
            {
                if (charFreqs[i] > 0)
                {
                	String le = Character.toString((char)(i));
                    trees.offer(new HuffmanLeaf(charFreqs[i], le));
                    stringFreqs.put(le, i);
                }
            }
        }
        if(num_symbols > 1)
        {
        	for (int i = 65; i < charFreqs.length; i++)
        	{
        		if (charFreqs[i] > 0)
        		{
        			String sy = Character.toString((char)(65 + i/26)) + Character.toString((char)(65 + i%26));
        			trees.offer(new HuffmanLeaf(charFreqs[i], sy));
        			stringFreqs.put(sy, i);
        			
        		}
        	}
        }

        assert trees.size() > 0;
        // loop until there is only one tree left
        while (trees.size() > 1) {
            // two trees with least frequency
            HuffmanTree a = trees.poll();
            HuffmanTree b = trees.poll();
 
            // put into new node and re-insert into queue
            trees.offer(new HuffmanNode(a, b));
        }
        return trees.poll();
    }
 
    public static void printCodes(HuffmanTree tree, StringBuffer prefix, Map<String, String> encds, Map<String, String> encdsr) {
    	
    	assert tree != null;
        if (tree instanceof HuffmanLeaf) {
            HuffmanLeaf leaf = (HuffmanLeaf)tree;
 
            System.out.println(leaf.value + "\t" + leaf.frequency + "\t" + prefix);
            
            encds.put(leaf.value , prefix.toString());
            
            encdsr.put(prefix.toString(), leaf.value);

 
        } else if (tree instanceof HuffmanNode) {
            HuffmanNode node = (HuffmanNode)tree;
 
            // traverse left
            prefix.append('0');
            printCodes(node.left, prefix, encds, encdsr);
            prefix.deleteCharAt(prefix.length()-1);
 
            // traverse right
            prefix.append('1');
            printCodes(node.right, prefix,encds, encdsr);
            prefix.deleteCharAt(prefix.length()-1);
        }
        
    }
 
}